import React, { Component } from 'react'
import "./post.css"

import HeartOutline from "./assets/HeartOutline.png"
import HeartFilled from "./assets/HeartFilled.png"
import CommentOutline from "./assets/comment.png"

import axios from 'axios';

export default class Post extends Component {

    state = {
        visible: false,
        post_id: null,
        content: null,
        post_date: null,
        like_count: null,
        community_name: null,
        if_curr_user_liked: 0,
        post_user_public_name: null,
        comment_count: null,
        title: null,
        new_comment: null
    }

    componentDidMount() {
        if(this.props.visible) {
            this.getPostInfo(this.props.post_id);
        }
    }

    componentWillUpdate(nextProps, nextState) {
        if(nextProps.visible !== this.props.visible || nextProps.post_id !== this.props.post_id) {
            const post_id = nextProps.post_id === undefined ? this.props.post_id : nextProps.post_id
            console.log(post_id, nextProps.visible)
            this.setState({
                post_id: post_id,
                visible: nextProps.visible,
                new_comment: ""
            }, () => this.getPostInfo(post_id));
        }
    }

    async getPostInfo(post_id) {
        const config = {
            method: 'get',
            url: `https://cs498-final-project-0llama.herokuapp.com/api/post/${post_id}`,
        }
        await axios(config)
        .then((res) => {
            const post = res.data[0]
            this.setState({
                content: post.Content,
                post_date: post.Date_Created,
                like_count: post.Like_Count,
                post_user_public_name: post.Public_Name,
                title: post.Title,
                community_name: post.Name,
                comment_count: post.Comment_Count,
                new_comment: ""
            });

            const config = {
                method: 'get',
                url: `https://cs498-final-project-0llama.herokuapp.com/api/like`,
                params: {
                    userID: this.props.user_id,
                    postID: post_id
                }
            }
            axios(config)
            .then((res) => {
                this.setState({
                    if_curr_user_liked: res.data ? 1 : 0,
                    new_comment: ""
                })
            });
        }).catch((err) => {
            console.log(err);
        })

        const commentConfig = {
            method: 'get',
            url: `https://cs498-final-project-0llama.herokuapp.com/api/comment/${post_id}`,
        }
        await axios(commentConfig)
        .then((res) => {            console.log(res.data)
            const comment_divs = res.data.map((item) => {
                return (
                    <div id="comment-div">
                        <div className="comment-subdiv">
                            <h4>{item.Public_Name === null ? "anonymous" : item.Public_Name}</h4>
                            <p>{item.Date_Created}</p>
                        </div>
                        <p>{item.Content}</p>
                    </div>
                )
            });
            this.setState({
                comments: comment_divs
            })
        }).catch((err) => {
            console.log(err);
        })
    }

    updatePostLike(post_id) {
        const config = {
            method: 'get',
            url: `https://cs498-final-project-0llama.herokuapp.com/api/like`,
            params: {
                userID: this.props.user_id,
                postID: post_id
            }
        }
        axios(config)
        .then((res) => {
            console.log(res.data);
            if(res.data) {
                const config = {
                    method: 'delete',
                    url: `https://cs498-final-project-0llama.herokuapp.com/api/like`,
                    params: {
                        userID: this.props.user_id,
                        postID: post_id
                    }
                }
                axios(config)
                .then((res) => {
                    console.log(res.data);
                    this.getPostInfo(this.state.post_id);
                })
                .catch((err) => {
                    console.log(err);
                })
            } else {
                const config = {
                    method: 'post',
                    url: `https://cs498-final-project-0llama.herokuapp.com/api/like`,
                    params: {
                        userID: this.props.user_id,
                        postID: post_id
                    }
                }
                axios(config)
                .then((res) => {
                    console.log(res.data);
                    this.getPostInfo(this.state.post_id);
                })
                .catch((err) => {
                    console.log(err);
                })
            }
        })
        .catch((err) => {
            console.log(err);
        })
    }

    postComment() {
        const config = {
            method: 'get',
            url: `https://cs498-final-project-0llama.herokuapp.com/api/community`,
            params: {
                userID: this.props.user_id,
                communityID: this.props.curr_community_id,
            }
        }
        axios(config)
        .then((res) => {
            const name = res.data[0].Public_Name;
            console.log("name " + name)
            const config = {
                method: 'post',
                url: `https://cs498-final-project-0llama.herokuapp.com/api/comment`,
                params: {
                    publicName: name === null ? "anonymous" : name,
                    postID: this.state.post_id,
                    content: this.state.new_comment
                }
            }
            axios(config)
            .then((res) => {
                this.getPostInfo(this.state.post_id)
            }).catch((err) => {
                console.log(err);
            })
        }).catch((err) => {
            console.log(err);
        })
    }

    render(){
    return(
        <div id="overlay-background" className={this.state.visible ? "visible" : "hidden"}>
            <div id="overlay-content" className="black-bottom-shadow">
            <div className="post-expanded-div">
                <div className="post-expanded-scroll-content">
                    <div className="flex-row">
                        <div className="post-content">
                            <h1>{this.state.post_user_public_name}</h1>
                            <br/>
                            <h4>{this.state.community_name}</h4>
                            <h4>{this.state.post_date}</h4>
                        </div>
                    <div id="post-expand-div">
                        <button onClick={() => this.props.closeOverlay()}>&#10005;</button>
                    </div>
                    </div>
                        <div id="post-text">
                            <p>{this.state.content}</p>
                        </div>
                    
                        <div className="post-action-items-div">
                            <div className="post-like-div cursor-pointer" onClick={() => this.updatePostLike(this.state.post_id)}>
                                <img src={this.state.if_curr_user_liked === 1 ? HeartFilled : HeartOutline} id="post-like-heart-img"/>
                                <p>{this.state.like_count}</p>
                            </div>
                            <div className="post-like-div">
                                <img src={CommentOutline} id="post-like-heart-img"/>
                                <p>{this.state.comment_count}</p>
                            </div>
                        </div>
                        <div id="post-comments">
                            {this.state.comments}
                        </div>
                        <div className="post-expanded-action-items-div">
                            <input type="text" value={this.state.new_comment} className="post-text-field" name="post_content" onChange={(e) => this.setState({new_comment: e.target.value})} />{' '}
                            <button onClick={() => this.postComment()}>share comment</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
    }
}