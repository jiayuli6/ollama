import React, {useEffect, useState} from "react";
import axios from "axios";

const JoinCommunity = (props) => {
    const [isVisible, setVisible] = useState(props.visible);
    const [communityName, setCommunityName] = useState("")
    const [password, setPassword] = useState("");
    const [actualPassword, setActualPassword] = useState("");

    useEffect(() => {
        setVisible(props.visible);

        const config = {
            method: 'get',
            url: `https://cs498-final-project-0llama.herokuapp.com/api/community/${props.community_id}`,
        }
        axios(config)
        .then((res) => {
            setCommunityName(res.data[0].Name);
            setActualPassword(res.data[0].Password);
        })
        .catch((err) => {
            console.log(err);
        })

    }, [props.visible]);

    const joinCommunity = () => {
        if(password === actualPassword) {
            console.log(props.user_id, props.community_id)
            const config = {
                method: 'post',
                url: `https://cs498-final-project-0llama.herokuapp.com/api/mycommunities`,
                params: {
                    userID: props.user_id,
                    communityID: props.community_id,
                    publicName: null
                }
            }
            axios(config)
            .then((res) => {
                console.log(res.data)
            })
            .catch((err) => {
                console.log(err);
            })
        }
    }

    return(
        <div id="overlay-background" className={isVisible ? "visible" : "hidden"}>
            <div id="overlay-content" className="black-bottom-shadow">
                <h1>Join {communityName}</h1>
                <div className="flex-row">
                    <label>Password</label>
                    <input type="password" id="password" defaultValue={password} onChange={(event) => setPassword(event.target.value)}/>
                </div>
                <div className="flex-row signin-action-div">
                    <button onClick={() => props.closeOverlay()}><p>Cancel</p></button>
                    <button onClick={() => {joinCommunity(); props.closeOverlay()}}><p>Join!</p></button>
                </div>
            </div>
        </div>    
    );
}

export default JoinCommunity;