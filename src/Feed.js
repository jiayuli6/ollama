import React from "react";
// import { withRouter } from 'react-router-dom';
import axios from "axios";
import './feed.css';

import NewPost from "./NewPost";
import Post from "./Post";
import CommunitySidebar from "./CommunitySidebar.js";
import InfoSidebar from "./InfoSidebar";
import FeedContent from "./FeedContent";
import NewCommunity from "./NewCommunity";
import JoinCommunity from "./JoinCommunity";

const TABLET_SIZE = 768;

export default class Feed extends React.Component {
  state = {
    user_id: null,
    sort_by_date: true,
    curr_community_your_name: "anonymous",
    curr_community_id: 0,
    show_new_post_overlay: false,
    show_post_expanded_overlay: null,
    show_create_new_community_overlay: false,
    show_join_new_community_overlay: false,
    show_join_community_overlay: null,
    show_create_new_community_overlay: false,
    show_info: false,
    show_comm: true,
    show_feed: false,
    width: window.innerWidth,
    your_communities: [],
    other_communities: [],
    posts: []
  }

  componentDidMount() {
    this.checkIfUserExists();
    window.history.replaceState(null, "", "/feed");

    if (window.innerWidth <= TABLET_SIZE) {
      this.setState({
        show_info: false,
        show_comm: false,
        show_feed: true
      })
    }
  }

  componentWillUpdate(nextProps, nextState) {
    if(nextState.user_id !== this.state.user_id) {
      this.checkIfUserExists();

      if (window.innerWidth <= TABLET_SIZE) {
        this.setState({
          show_info: false,
          show_comm: false,
          show_feed: true
        });
      }
    }
  }

  checkIfUserExists() {
    const axiosConnection = axios.create({
      baseURL: `https://cs498-final-project-0llama.herokuapp.com/`
    });

    // check if user_id is in USER database
    axiosConnection.get(`/api/user/${this.props.user_id}`, {})
    .then((res) => {
      if(res.data === 1) {
        this.setState({
          user_id: this.props.user_id
        }, () => this.loadPage());
      } else {
        // if it is not, redirect to auth
        this.props.history.push("/");
      }
    });
  }

  loadPage() {
    const axiosConnection = axios.create({
      baseURL: `https://cs498-final-project-0llama.herokuapp.com/`
    });

    // get your communities
    axiosConnection.get(`/api/mycommunities/${this.state.user_id}`, {})
    .then((res) => {
      console.log(res.data)
      // update your_communities and curr_community with res info
      this.setState({
        your_communities: res.data
      })
    })
    .catch((err) => {
      console.log(err);
    });

    // get other communities
    axiosConnection.get(`/api/communities/${this.state.user_id}`, {})
    .then((res) => {
      this.setState({
        other_communities: res.data
      })
    })
    .catch((err) => {
      console.log(err);
    });
  }

  setCurrComm = (comm) => {
    this.setState({
      curr_community_id: comm.Community_id,
      curr_community_name: comm.Name,
      show_feed: this.state.width >= TABLET_SIZE ? true : false,
      show_info: this.state.width >= TABLET_SIZE ? true : false
    })

    this.setCurrCommInfo(comm.Community_id);
  }

  setCurrCommInfo = (comm_id) => {
    const axiosConnection = axios.create({
      baseURL: `https://cs498-final-project-0llama.herokuapp.com/`
    });

    axiosConnection.get("/api/community/" + comm_id, {})
      .then((res) => {
        console.log(res);

        this.setState({
          curr_community_num_users: res.data[0].User_Count,
          curr_community_password: res.data[0].Password
        })
      })
      .catch((err) => {
        console.log(err);
      })
  }

  componentWillMount() {
    window.addEventListener('resize', this.handleWindowSizeChange);
  }
  
  componentWillUnmount() {
    window.removeEventListener('resize', this.handleWindowSizeChange);
  }

  handleWindowSizeChange = () => {
    if (this.state.width <= TABLET_SIZE && window.innerWidth <= TABLET_SIZE) {
    } else if (this.state.width <= TABLET_SIZE && window.innerWidth > TABLET_SIZE) {
      this.setState({
        show_info: true,
        show_comm: true,
        show_feed: true
      })
    } else if (this.state.width > TABLET_SIZE && window.innerWidth > TABLET_SIZE) {}
    else {
      this.setState({
        show_info: false,
        show_comm: false,
        show_feed: true
      })
    }
    this.setState({ width: window.innerWidth });
  }

  setAnon = () => {
    this.setState({
      curr_community_anon: !this.state.curr_community_anon
    })
  }

  setName = (name) => {
    this.setState({
      curr_community_name: name
    })
  }

  setUserId = (id) => {
    this.setState({
      user_id: id
    })
  }

  setPostExpandedOverlay = (id) => {
    this.setState({
      show_post_expanded_overlay: id
    })
  }

  setNewPostOverlay = (isVisible) => {
    this.setState({
      show_new_post_overlay: isVisible
    })
  }

  setJoinCommOverlay = (id) => {
    this.setState({
      show_join_community_overlay: id
    })
  }

  setNewCommOverlay = (isVisible) => {
    this.setState({
      show_create_new_community_overlay: isVisible
    })
  }

  setShowComm = (isVisible) => {
    if (isVisible) {
      this.setState({
        show_comm: true,
        show_feed: false
      })
    } else {
      this.setState({
        show_comm: false,
        show_feed: true
      })
    }
  }

  setShowInfo = (isVisible) => {
    if (isVisible) {
      this.setState({
        show_info: true,
        show_feed: false
      })
    } else {
      this.setState({
        show_info: false,
        show_feed: true
      })
    }
  }

  goBack = () => {
    this.setState({
      show_info: false,
      show_feed: true,
      show_comm: false
    })
  }

  onSortOrderChange = (sort_by_date) => {
    this.setState({
      sort_by_date: sort_by_date
    });
  }

  render() {
    const { width } = this.state;
    const isMobile = width <= TABLET_SIZE;

      return (
        <div className="App">
        <NewPost  visible={this.state.show_new_post_overlay} 
                  user_id = {this.state.user_id}
                  community_id = {this.state.curr_community_id}
                  closeOverlay={() => {this.setState({show_new_post_overlay: false, show_feed: !this.state.show_feed}, () => this.setState({show_feed: !this.state.show_feed}, () => this.loadPage())); this.loadPage()}}/>
        <JoinCommunity visible={this.state.show_join_community_overlay=== null ? false : true}
                  community_id={this.state.show_join_community_overlay}
                  user_id={this.state.user_id}
                  closeOverlay={() => {this.setState({show_join_community_overlay: null, show_feed: !this.state.show_feed}, () => this.setState({show_feed: !this.state.show_feed}, () => this.loadPage())); this.loadPage()}}/>
        <NewCommunity visible={this.state.show_create_new_community_overlay}
                  user_id = {this.state.user_id}
                  closeOverlay={() => {this.setState({show_create_new_community_overlay: false}); this.loadPage();}}/>
        <Post visible={this.state.show_post_expanded_overlay=== null ? false : true}
            post_id={this.state.show_post_expanded_overlay}
            user_id={this.state.user_id}
            curr_community_id={this.state.curr_community_id}
            closeOverlay={() => {this.setState({show_post_expanded_overlay: null, show_feed: !this.state.show_feed}, () => this.setState({show_feed: !this.state.show_feed}, () => this.loadPage())); this.loadPage()}}/>
        <div className="app-content">
          <CommunitySidebar isMobile = {isMobile}
            visible = {this.state.show_comm}
            goBack = {this.goBack}
            setCurrComm = {this.setCurrComm}
            curr_community_id = {this.state.curr_community_id}
            your_communities = {this.state.your_communities}
            other_communities = {this.state.other_communities}
            setJoinCommOverlay = {this.setJoinCommOverlay}
            setNewCommOverlay = {this.setNewCommOverlay}/>
          <FeedContent isMobile = {isMobile}
            visible = {this.state.show_feed}
            setShowComm = {this.setShowComm}
            setShowInfo = {this.setShowInfo}
            setPostExpandedOverlay = {this.setPostExpandedOverlay}
            setNewPostOverlay = {this.setNewPostOverlay}
            sort_by_date = {this.state.sort_by_date}
            onSortOrderChange = {this.onSortOrderChange}
            curr_community_id={this.state.curr_community_id}
            user_id={this.state.user_id}/>

          <InfoSidebar isMobile = {isMobile}
            visible = {this.state.show_info}
            goBack = {this.goBack}
            curr_community_id={this.state.curr_community_id}
            user_id={this.state.user_id}
            reloadPage={() => {this.setState({show_info: false, show_feed: false}, () => this.loadPage()); this.loadPage()}}/>
        </div>
        </div>
      );
  }
}
